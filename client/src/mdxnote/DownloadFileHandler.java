/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.util.ArrayList;

/**
 *
 * @author nurma
 */
public class DownloadFileHandler {
    
    private ArrayList<ArrayList<String>> data = new ArrayList();
    private int running = 0, completed = 0, ran = 0;
    
    public DownloadFileHandler(ArrayList<ArrayList<String>> data){
        //data in this order => url, name
        this.data = data;
        initiate();
    }
    
    public DownloadFileHandler(){
    
    }
    
    public void addData(ArrayList<String> data){
        this.data.add(data);
        initiate();
    }
    
    private synchronized void initiate(){
        if(ran < data.size() && running < 10){    
            new DownloadFile(data.get(ran).get(0), data.get(ran).get(1), this, ran).start();
            running ++;
            ran++;
            
            initiate();
        }
        
    }
    
    public void success(){
        completed++;
        running--;
        initiate();
    }
    
    public synchronized void html(int i){
        System.out.println("Reissuing HTML download... @ " + data.get(i).get(0));
        HTTPClient hc = new HTTPClient(false);
        String body = hc.HTTPNormal(true, data.get(i).get(0), true, true, true, false);
        int urlStart = body.indexOf("<a href=\"https://mdx.mrooms.net/pluginfile.php") + 9;
        //System.out.println(body);
        String url;
        url = body.substring(urlStart, body.indexOf("\"", urlStart));
        ArrayList<String> newData = new ArrayList();
        newData.add(url);
        newData.add(data.get(i).get(1));
        this.addData(newData);
    }
}
