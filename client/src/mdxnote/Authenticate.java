/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author DoWhile
 */
public class Authenticate {
    static String[] credentials = {"",""};
    static Dimension screenSize= Toolkit.getDefaultToolkit().getScreenSize();
    
    public Authenticate(){
        
    }
    
    public int check(){
        //checks whether the user's login credentials are saved and prompts the user to give the details if ever missing
        
        String line = null;
        String fileName = "credentials.alice";
        System.out.println("looking for file at "+new File(fileName).getAbsolutePath());
        try {
            FileReader fileReader = 
                new FileReader(fileName);

            BufferedReader bufferedReader = new BufferedReader(fileReader);
            int count = 0;
            while((line = bufferedReader.readLine()) != null) {
                System.out.println(line);
                credentials[count] = line;
                count ++;
            }
            bufferedReader.close(); 
            
            return 0;
        }
        catch(FileNotFoundException ex) {
            System.out.println("Login Credentials missing. Login required...");
            return 1;
        }
        catch(IOException ex) {
            ex.printStackTrace();
            return 2;
        }
    }
    
    public void setCredentials(String[] cr){
        credentials = cr;
    }
    
    public static String[] getCredentials(){
        return credentials;
    }
    
    public static Dimension getScreenSize(){
        return screenSize;
    }
}
