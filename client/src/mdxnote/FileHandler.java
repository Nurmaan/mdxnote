/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nurma
 */
public class FileHandler {
    
    public FileHandler(){
    
    }
    
    public static boolean checkFile(String url){
        File f = new File(url);
        if(f.exists() && !f.isDirectory()){
            return true;
        }
        return false;
    }
    
    public static void newFile(String name, String extension){
        FileWriter fw = null;
        try {
            File f = new File("entries.txt");
            fw = new FileWriter(f, true);
            String data = name + "," + name + extension;
            fw.write(data);
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(FileHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
