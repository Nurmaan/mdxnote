/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author nurma
 */
public class FileExtensionFromMIME {
    
    private static Map<String, String> map = new HashMap<String, String>();
    
    public FileExtensionFromMIME(){
        

    }
    
    public static String getExtension(String key){
        
        map.put("application/vnd.ms-powerpoint",".ppt"); 
        map.put("application/vnd.openxmlformats-officedocument.wordprocessingml.document",".docx");
        map.put("application/msword",".doc");
        map.put("application/pdf",".pdf");
        map.put("application/vnd.ms-excel",".xls");
        map.put("application/vnd.ms-excel.addin.macroenabled.12",".xlam");
        map.put("application/vnd.ms-excel.sheet.binary.macroenabled.12",".xlsb");
        map.put("application/vnd.ms-excel.sheet.macroenabled.12",".xlsm");
        map.put("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",".xlsx");
        map.put("application/x-msaccess	",".mdb");
        map.put("video/x-ms-asf",".asf");
        map.put("application/x-msdownload",".exe");
        map.put("application/vnd.ms-artgalry",".cil");
        map.put("application/vnd.ms-cab-compressed",".cab");
        map.put("application/vnd.ms-ims",".ims");
        map.put("application/x-ms-application",".application");
        map.put("application/x-msclip",".clp");
        map.put("image/vnd.ms-modi",".mdi");
        map.put("application/vnd.ms-fontobject",".eot");
        map.put("application/vnd.ms-powerpoint.addin.macroenabled.12",".ppam");
        map.put("application/vnd.ms-powerpoint.slide.macroenabled.12",".sldm");
        map.put("application/vnd.ms-powerpoint.presentation.macroenabled.12",".pptm");
        map.put("application/vnd.ms-powerpoint.slideshow.macroenabled.12",".ppsm");
        map.put("application/vnd.ms-powerpoint.template.macroenabled.12",".potm");        
        map.put("application/vnd.visio",".vsd");
        map.put("application/vnd.visio2013",".vsdx");
        map.put("application/msaccess",".accdb");        
        map.put("video/x-ms-wm",".wm");
        map.put("audio/x-ms-wma",".wma");
        map.put("audio/x-ms-wax",".wax");
        map.put("video/x-ms-wmx",".wmx");
        map.put("application/x-ms-wmd",".wmd");
        map.put("application/vnd.ms-wpl",".wpl");
        map.put("application/vnd.ms-word.document.macroenabled.12",".docm");
        map.put("application/vnd.ms-word.template.macroenabled.12",".dotm");
        map.put("application/x-mswrite",".wri");
        map.put("application/vnd.openxmlformats-officedocument.presentationml.presentation",".pptx");
        map.put("application/vnd.openxmlformats-officedocument.presentationml.slide",".sldx");
        map.put("application/vnd.openxmlformats-officedocument.presentationml.slideshow",".ppsx");
        map.put("text/html",".html"); 
        map.put("text/plain",".txt");
        map.put("text/rtf",".rtf");



        
        return map.get(key);
    }
}
