/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.util.ArrayList;

/**
 *
 * @author nurma
 */
public class getWeeksHandler {
    
    private int running = 0;
    private int ran = 0;
    private int completed  = 0;
    private ArrayList<ArrayList> data = new ArrayList();
    
    public getWeeksHandler(ArrayList<ArrayList> data){
        // data in the form [[name, url],[name, url],....]
        this.data = data;
        if (!this.data.isEmpty()){
            initiate();
        }
    }
    
    public getWeeksHandler(){
    
    }
    
    public void addData(ArrayList<String> newData){
        data.add(newData);
        initiate();
    }
    
    private void initiate(){
        if(running < 10 && ran < data.size()){
           //initiate getWeeks
           new getWeeks(this, data.get(ran).get(0).toString(), data.get(ran).get(1).toString()).start();
           running ++;
           ran++;
           initiate();
     
        }
    }
    
    public void complete(){
        running--;
        completed++;
        initiate();
    }
    
    public void getNotes(){
        
    }

}
