/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.util.ArrayList;

/**
 *
 * @author nurma
 */
public class GetNotes extends Thread{
    private final String module;
    private final String week;
    private final String url;
    private final HTTPClient hc = new HTTPClient(false);
    private final DownloadFileHandler dfh;
    
    @Override
    public void run(){
        String response = hc.HTTPNormal(true, url, true, true, true, false);
        int lastPos = 0, firstPos;
        System.out.println("Looking for notes in folder: " + week);
        while((firstPos = response.indexOf("<span class=\"fp-filename-icon\">", lastPos)) != -1){
            lastPos = firstPos + 20;
            String noteName, noteUrl;
            int noteUrlStart = response.indexOf("<a href=\"https://mdx.mrooms.net/pluginfile.php", lastPos) + 9;
            int noteNameStart = response.indexOf("title=\"", noteUrlStart) + 7;
            noteUrl = response.substring(noteUrlStart, response.indexOf("\"", noteUrlStart)); 
            noteName = response.substring(noteNameStart, response.indexOf("\"", noteNameStart)).replaceAll("[\\\\/:*?\"<>|]", "").replaceAll(" ", "_");;
            ArrayList<String> data = new ArrayList();
            data.add(noteUrl);
            data.add(module+"\\"+week+"\\"+noteName);
            System.out.println("Initiating download of " + module+"\\"+week+"\\"+noteName);
            dfh.addData(data);
            
        }
        
    }
    
    public GetNotes(String module, String week, String url, DownloadFileHandler dfh){
        this.module = module.replaceAll("[\\\\/:*?\"<>|]", "").replaceAll(" ", "_");
        this.week = week.toLowerCase().replaceAll("[\\\\/:*?\"<>|]", "").replaceAll(" ", "_");
        this.url = url;
        this.dfh = dfh;
    }
}
