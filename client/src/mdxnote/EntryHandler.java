/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author nurma
 */
public class EntryHandler {  
    
    private final File entry = new File("Entry.txt");
    private static Map<Integer, ArrayList<String>> entries  = new HashMap();
    private static ArrayList<String> checksums;

    
    public EntryHandler(){
        FileWriter fw = null;
        try {
            fw = new FileWriter(entry, false);
            fw.write("");
            try {
                entry.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) {
            Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fw.close();
            } catch (IOException ex) {
                Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void setEntry(String location){    
        fetchDirectory(location);
    }
    
    public String getEntry(){
        String ret = "";
        return ret;
    
    }
    
    private void fetchDirectory(String dir){
        System.out.println("Working with directory " + dir);
        File f = new File(dir);
        String[] dirs = f.list();
        for(int i = 0; i < dirs.length; i++){
            String newDir = dir + "\\" + dirs[i];
            String entryData;
            File f1 = new File(newDir);
            if(f1.isDirectory()){
                System.out.println("\tFound new Directory " + dirs[i]);
                fetchDirectory(newDir);
            }else{
                
                System.out.println("\tOriginal Name: " + dirs[i]);
                int index = dirs[i].lastIndexOf(".");
                String name = dirs[i].substring(0, index > 0 ? index : dirs[i].length());
                String checksum = getChecksum(newDir);
                System.out.println("Checksum: " + checksum);
                System.out.println("\tName without Extension: " + name + "\n");
                write(dir + "\\" + name + "," + newDir + "," + checksum);
            }
        }
    }
    
    private void write(String data){
        try {
            FileWriter fw = new FileWriter(entry, true);
            BufferedWriter bw = new BufferedWriter(fw);
            bw.write(data);
            bw.newLine();
            bw.close();
            fw.close();
        } catch (IOException ex) {
            Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private String getChecksum(String filePath){
        String myChecksum = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(Files.readAllBytes(Paths.get(filePath)));
            byte[] digest = md.digest();
            myChecksum = DatatypeConverter.printHexBinary(digest).toUpperCase();
        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return myChecksum;
    }
    
    public static String getStaticChecksum(String filePath){
        String myChecksum = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(Files.readAllBytes(Paths.get(filePath)));
            byte[] digest = md.digest();
            myChecksum = DatatypeConverter.printHexBinary(digest).toUpperCase();
        } catch (NoSuchAlgorithmException | IOException ex) {
            Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return myChecksum;
    }
    
    public static void setStaticEntry(){
        try {
            Scanner sc = new Scanner(new File("Entry.txt"));
            int count = 0;
            entries.clear();
            checksums.clear();
            //Map<Integer, ArrayList> entries = new HashMap();
            while(sc.hasNextLine()){
                ArrayList<String> content = new ArrayList();
                
                String[] split = sc.nextLine().split(",");
                content.add(split[0]);
                content.add(split[1]);
                content.add(split[2]);
                checksums.add(split[2]);

                entries.put(count, content);
                count++;
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(EntryHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static ArrayList<String> getChecksums(){    
        return checksums;
    }
    
    
    public static Map<Integer, ArrayList<String>> getStaticEntry(){
        return entries;
    }
}
