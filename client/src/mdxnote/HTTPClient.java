/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote; 
/**
 *
 * @author nurma
 */
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;

public class HTTPClient {
    
    private ArrayList<String> cookies = new ArrayList();
    private Map cookiesMap = new HashMap();
    private String location = null;
    private CloseableHttpResponse response = null;
    private ByteArrayOutputStream baos = new ByteArrayOutputStream(); 
    private String contentType = "NONE";
    
    public HTTPClient(boolean erase){
        //setCookie();
        if(erase){
            PrintWriter writer;
            try {
                writer = new PrintWriter(new File("cookie.txt"));
                writer.print("");
                writer.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(HTTPClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
    
    public String HTTPNormal(boolean write, String url, boolean follow, boolean circular, boolean type, boolean fileWrite){

        String body = "";
        CloseableHttpClient client = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);

        RequestConfig defaultRequestConfig = RequestConfig.custom().setRedirectsEnabled(follow).setCircularRedirectsAllowed(circular).build();
        
        httpGet.setConfig(defaultRequestConfig);

        setCookie();
        if(type){
            String tempCookie = "";
            /*for(int i=0; i < cookies.size(); i++){
                tempCookie += cookies.get(i) + ";";
            }
            httpGet.setHeader("Cookie", tempCookie);*/
            
            Iterator it = cookiesMap.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry)it.next();
                tempCookie += pair.getKey() + "=" + pair.getValue()+";";
                it.remove();
            }
            httpGet.setHeader("Cookie", tempCookie);
        }else{
            for(int i=0; i < cookies.size(); i++){
               httpGet.setHeader("Cookie: ", cookies.get(i));
            }
        }
       
        
       
        //System.out.println("Cookie in request: " + Arrays.toString(httpGet.getHeaders("Cookie")));
        

        try {
            response = client.execute(httpGet);
            if(fileWrite){
                HttpEntity entity = response.getEntity();
                entity.writeTo(baos);
                this.contentType = Arrays.toString(response.getHeaders("Content-Type"));
                //System.out.println("All headers for " + url + " " + Arrays.toString(response.getAllHeaders()));
                
            }else{
                ResponseHandler<String> handler = new BasicResponseHandler();
                try{
                    body = handler.handleResponse(response);

                }catch(HttpResponseException e){
                    //System.out.println("HttpResponseException");
                }
                String cookie = Arrays.toString(response.getHeaders("Set-Cookie"));
                //System.out.println("set_cookie: "+ cookie);
                location = Arrays.toString(response.getHeaders("Location"));
                client.close();


                if(write && cookie != "[]"){
                    try(FileWriter fw = new FileWriter("cookie.txt", true);
                            BufferedWriter bw = new BufferedWriter(fw);
                            PrintWriter out = new PrintWriter(bw)){
                        out.println(cookie);
                    }catch (IOException e) {
                        System.out.println("Error occured");
                    }
                    //System.out.println(body);
                    //System.out.println(cookie);
                }
            }
            
           

        } catch (IOException ex) {
            Logger.getLogger(HTTPClient.class.getName()).log(Level.SEVERE, null, ex);
        }

        return body;
    }
    
    public String HTTPPost(boolean write, String url, String[][] parameters, boolean follow){
        CloseableHttpClient client = HttpClients.createDefault();
        String body = "";
        HttpPost httpPost = new HttpPost(url);
        
        
        HttpParams parames;
        parames = new BasicHttpParams();
        parames.setParameter(ClientPNames.HANDLE_REDIRECTS, follow);

        List<NameValuePair> params = new ArrayList<>();
        for(int i=0; i < parameters.length; i++){
            params.add(new BasicNameValuePair(parameters[i][0], parameters[i][1]));
        }
        
        try {
            httpPost.setEntity(new UrlEncodedFormEntity(params));
            httpPost.setParams(parames);
            setCookie();
            if(true){
                String tempCookie = "";
                /*for(int i=0; i < cookies.size(); i++){
                    tempCookie += cookies.get(i) + ";";
                }
                httpGet.setHeader("Cookie", tempCookie);*/

                Iterator it = cookiesMap.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry pair = (Map.Entry)it.next();
                    tempCookie += pair.getKey() + "=" + pair.getValue()+";";
                    it.remove();
                }
                httpPost.setHeader("Cookie", tempCookie);
            }
            System.out.println("Cookie in request: " + Arrays.toString(httpPost.getHeaders("Cookie")));
            
            try {
                response = client.execute(httpPost);                
                ResponseHandler<String> handler = new BasicResponseHandler();
                location = Arrays.toString(response.getHeaders("Location"));
                try{
                    body = handler.handleResponse(response);
                    
                }catch(HttpResponseException e){
                    //System.out.println("HttpResponseException");
                }
                
                String cookie = Arrays.toString(response.getHeaders("Set-Cookie"));
                    //System.out.println("set_cookie: "+ cookie);

                    if(write  && cookie != "[]"){
                        try(FileWriter fw = new FileWriter("cookie.txt", true);
                            BufferedWriter bw = new BufferedWriter(fw);
                            PrintWriter out = new PrintWriter(bw)){
                            out.println(cookie);
                        }catch (IOException e) {
                            System.out.println("Error occured");
                        }
                    }

                    //System.out.println(body);
                    //System.out.println(cookie);
                    client.close();
                
            } catch (IOException ex) {
                Logger.getLogger(HTTPClient.class.getName()).log(Level.SEVERE, null, ex);
            }
           
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(HTTPClient.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return body;
    }
    
    public void setCookie(){
        try(FileReader fw = new FileReader("cookie.txt")){
                BufferedReader bw = new BufferedReader(fw);
                String line;    
                while((line = bw.readLine()) != null){
                    
                    String cookie = "";  
                    line = line.replaceAll("\\[", "").replaceAll("\\]","");
                    String[] cookieArr = line.split("Set-Cookie: ");
                    //System.out.println("CookieArr " + Arrays.toString(cookieArr));
                    for(int i = 0; i < cookieArr.length; i++){
                        if(!cookieArr[i].equals("")){
                            cookie = cookieArr[i].substring(0, cookieArr[i].indexOf(";"));
                            String[] cookieSplit = cookie.split("=");
                            if(cookiesMap.get(cookieSplit[0]) != null){
                                cookiesMap.remove(cookieSplit[0]);
                            }
                            cookiesMap.put(cookieSplit[0], cookieSplit[1]);
                            
                            
                        }
                        
                    }                      
                    
                   
                    
                }
                //System.out.println("cookies: " + cookies);
                
        
        }catch (IOException e) {
                //exception handling left as an exercise for the reader
            }
    
    }
    
    public String getLocation(){
        return this.location;
    }
    
    public CloseableHttpResponse getResponse(){
        return this.response;
    }
    
    public ByteArrayOutputStream getBaos(){
        return this.baos;
    }
    
    public String getContentType(){
        return this.contentType;
    }
}
