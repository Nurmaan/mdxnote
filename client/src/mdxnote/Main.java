/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.awt.Dimension;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SplitPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author DoWhile
 */
public class Main {
    Dimension screenSize;
    SplitPane root;
    final VBox module = new VBox();
    final StackPane moduleContainer = new StackPane();
    final StackPane week = new StackPane();
    final StackPane note = new StackPane();
    public Main(){
        screenSize = Authenticate.getScreenSize();
    }
    
    public Scene getScene(){
        //creating new components
        root = new SplitPane();
        
        final Label moduleTitle = new Label("Modules");
        final Label weekTitle = new Label("Weeks");        
        final Label noteTitle = new Label("Notes");        
        
        final ScrollPane moduleScroll = new ScrollPane();
        final ScrollPane weekScroll = new ScrollPane();
        final ScrollPane noteScroll = new ScrollPane();

        Button alter = new Button("Create");
        
        //modifying components
        module.setAlignment(Pos.BASELINE_CENTER);
        module.getChildren().add(moduleTitle); 
        module.getChildren().add(alter);
        module.setAlignment(Pos.CENTER);
        moduleContainer.getChildren().add(module);
        moduleContainer.setAlignment(module, Pos.CENTER);
        week.getChildren().add(weekTitle);
        note.getChildren().add(noteTitle);
        
        
        moduleTitle.setAlignment(Pos.CENTER);
        weekTitle.setAlignment(Pos.CENTER);
        noteTitle.setAlignment(Pos.CENTER);
        
        moduleScroll.setContent(moduleContainer);  
        weekScroll.setContent(week);
        noteScroll.setContent(note);

        
        root.getItems().addAll(moduleScroll, weekScroll, noteScroll);
        root.setDividerPositions(0.3, 0.6, 0.9);
        
        //Action Listeners
        alter.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                alter();
            }
        });
        
        //setting scene and returning
        Scene scene = new Scene(root, (screenSize.width)/1.2, (screenSize.height)/1.2);
        return scene;
    }
    
    public void alter(){
        Button b =new Button("new button for testing");
        module.getChildren().add(b);
        
        b.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event){
                alter();
            }
        });
    }
}
