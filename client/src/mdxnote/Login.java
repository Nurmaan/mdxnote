/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.awt.Dimension;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;

/**
 *
 * @author DoWhile
 */
public class Login{
    Dimension screenSize;
    public Login(){
        screenSize = Authenticate.getScreenSize();
    }
    
    public Scene getScene(){ 
        
        
        TextField TFUsername = new TextField();
        PasswordField PFPassword = new PasswordField();
        Label LblUsername = new Label ("Username");
        Label LblPassword = new Label("Password");
        Button confirm = new Button("Confirm");
        
        
        confirm.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                //getting user credentials
        String username = TFUsername.getText();
        String password = String.valueOf(PFPassword.getText());
        System.out.println("Username: "+username+"\n\rPassword: "+password);
        System.out.println(password.equals(""));
        if(!username.equals("") && !password.equals("")){
            //writing to file
            String fileName = "credentials.alice";
            try {
                FileWriter fileWriter =
                    new FileWriter(fileName);

                BufferedWriter bufferedWriter =
                    new BufferedWriter(fileWriter);

                bufferedWriter.write(username);
                bufferedWriter.newLine();
                bufferedWriter.write(password);            
                bufferedWriter.close();                
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText("Your credentials have been successfully saved locally at "+new File(fileName).getAbsolutePath());
                alert.setTitle("Alice Systems");
                alert.showAndWait();
                new Main();
                //mainFrame.dispatchEvent(new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING));
            }
            catch(IOException ex) {
                System.out.println(
                    "Error writing to file '"
                    + fileName + "'");
                ex.printStackTrace();
            }
        }else{
            //JOptionPane.showMessageDialog(null, "You have not provided all the details. Please make sure you have typed in a username and a password.");
            System.out.println("You have not provided all the details. Please make sure you have typed in a username and a password.");
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setHeaderText("You have not provided all the details. Please make sure you have typed in a username and a password.");
            alert.setTitle("Alice Systems");
            alert.showAndWait();
        }
            }
        });
        
        
        
        
        GridPane root = new GridPane();
        
        root.setVgap(25);
        root.setHgap(15);
        root.setPadding(new Insets(20,20,20,20));
        root.setAlignment(Pos.CENTER);
        root.setBackground(new Background(new BackgroundFill(Color.web("#cacaca"), CornerRadii.EMPTY, Insets.EMPTY)));
        
        root.add(LblUsername, 0, 0);
        root.add(TFUsername, 1, 0);
        root.add(LblPassword, 0, 1);
        root.add(PFPassword, 1, 1);
        root.add(confirm, 1, 2);
        
        Scene scene = new Scene(root, (screenSize.width)/2, (screenSize.height)/2);
        return scene;
    }
}
