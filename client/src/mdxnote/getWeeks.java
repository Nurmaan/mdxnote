/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author nurma
 */
public class getWeeks extends Thread{
    
    private final getWeeksHandler gwh;
    private final String url;
    private String name;
    private final DownloadFileHandler dfh;
    
    public getWeeks(getWeeksHandler gwh, String name, String url){
        this.gwh = gwh;
        this.url = url;
        this.name = name;
        this.dfh = new DownloadFileHandler();
    }
    
    @Override
    public void run(){
        System.out.println("Accessing Course" + name + " @ " + url);
        HTTPClient hc = new HTTPClient(false);
        String body = hc.HTTPNormal(true, url, true, true, true, false);
        getWeeks(body);
    }
    
    private void getWeeks(String body){
        System.out.println("Loading Folders For Course " + name);
        directLink(body);
        routedLink(body);
        
        gwh.complete();
        
    }
    
    private void routedLink(String body){
        System.out.println("Checking for routed links for "+name+" @ " + url);
        int lastPos = 0;
        
        while((lastPos=body.indexOf("<div class=\"activityinstance\"><a class=\"\" onclick=\"\" href=\"https://mdx.mrooms.net/mod/folder/view.php?id=", lastPos)) != -1){
                        
            ArrayList<String> content = new ArrayList();
            String urlLocal, nameLocal;    
            
            lastPos += 53;
            
            int urlStart = body.indexOf("href=\"", lastPos) + 6;
            int nameStart = body.indexOf("<span class=\"instancename\">", lastPos) + 27;
            nameLocal = body.substring(nameStart, body.indexOf("<", nameStart));
            urlLocal = body.substring(urlStart, body.indexOf("\"", urlStart));
            System.out.println("\t" + nameLocal + " @ " + urlLocal);
            
            new GetNotes(name, nameLocal, urlLocal, dfh).start();
        }
        
        System.out.println("Routed link completed for "+name+" @ " + url);
     }
    
    private void directLink(String body){
        System.out.println("Checking for direct links for "+name+" @ " + url);
        String[] weeks = body.split("<span class=\"toggle_");
        System.out.println("Found a total of " + (weeks.length-1) + " weeks");
        
        for(int i = 1; i < weeks.length; i++){
            String week, noteName, noteUrl, value = weeks[i];
            
            //get week name
            int weekStart = value.indexOf("<h3 class=\"sectionname\">") + 24;
            week = value.substring(weekStart, value.indexOf("<", weekStart));
            
            System.out.println("Working on " + week);
            
            //get note name and url
            int lastPos = 0;
            int firstPos = value.indexOf("<div class=\"activityinstance\"><a class=\"\" onclick=\"\" href=\"https://mdx.mrooms.net/mod/resource", lastPos);
            int secondPos = value.indexOf("<div class=\"activityinstance\"><a class=\"\" onclick=\"window.open(&#039;https://mdx.mrooms.net/mod/resource", lastPos);
            if(firstPos == -1 && secondPos == -1){
                secondPos = -1;
            }else if(firstPos == -1 && secondPos > 0){
                firstPos = secondPos;
            }else if(secondPos == -1 && firstPos > 0){
                secondPos = firstPos;
            }
        
            while((lastPos = firstPos > secondPos ? secondPos : firstPos) != -1){
                
                ArrayList<String> content = new ArrayList();
                String urlLocal, nameLocal;    

                lastPos += 53;

                int urlStart = value.indexOf("href=\"", lastPos) + 6;
                int nameStart = value.indexOf("<span class=\"instancename\">", lastPos) + 27;
                nameLocal = value.substring(nameStart, value.indexOf("<", nameStart));
                urlLocal = value.substring(urlStart, value.indexOf("\"", urlStart));
                System.out.println("\t" + nameLocal + " @ " + urlLocal);
                
                name = name.replaceAll("[\\\\/:*?\"<>|]", "").replaceAll(" ", "_");
                week = week.replaceAll("[\\\\/:*?\"<>|]", "").replaceAll(" ", "_");
                week = week.toLowerCase();
                nameLocal = nameLocal.replaceAll("[\\\\/:*?\"<>|]", "").replaceAll(" ", "_");

                content.add(urlLocal);
                content.add(name+"\\"+week+"\\"+nameLocal);                 
                
                //downlaod files
                dfh.addData(content);
                
                firstPos = value.indexOf("<div class=\"activityinstance\"><a class=\"\" onclick=\"\" href=\"https://mdx.mrooms.net/mod/resource", lastPos);
                secondPos = value.indexOf("<div class=\"activityinstance\"><a class=\"\" onclick=\"window.open(&#039;https://mdx.mrooms.net/mod/resource", lastPos);
                if(firstPos == -1 && secondPos == -1){
                    secondPos = -1;
                }else if(firstPos == -1 && secondPos > 0){
                    firstPos = secondPos;
                }else if(secondPos == -1 && firstPos > 0){
                    secondPos = firstPos;
                }
            }
            
        }
        
        System.out.println("Direct links completed for "+name+" @ " + url);
    }
    
}
