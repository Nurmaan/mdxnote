/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

/**
 *
 * @author nurma
 */
public class Mrooms {
    private String body;

    public Mrooms(){
        
    }
    
    public void begin(){
        HTTPClient hc = new HTTPClient(true);
        //initilise cookie
        System.out.println("Getting Cookies...");
        String body = hc.HTTPNormal(true, "https://myunihub.mdx.ac.uk/", true, true, true, false);
        String lt = body.substring(body.indexOf("<input type=\"hidden\" name=\"lt\" value=\"") + 38, body.indexOf("\"", body.indexOf("<input type=\"hidden\" name=\"lt\" value=\"") + 38));
        //System.out.println(lt);        
        login(hc, lt);
    }
    
    private void login(HTTPClient hc, String lt){
        //login to system
        System.out.println("Logging in to https://myunihub.mdx.ac.uk/");
        String[][] parameters = {{"username","NR588"}, {"password","M00683633mar"}, {"lt",lt}, {"execution","e1s1"}, {"_eventId","submit"}, {"submit_form","LOGIN"}};
        hc.HTTPPost(true, "https://myunihub-1.mdx.ac.uk/cas-web/login?service=https%3A%2F%2Fmyunihub.mdx.ac.uk%2Fc%2Fportal%2Flogin", parameters, false);
        
        mrooms(hc);
    }
    
    private void mrooms(HTTPClient hc){
        //Login to mrooms 
        System.out.println("Logging in to https://mdx.mrooms.net/");
        String redirect = "https://mdx.mrooms.net/login/index.php?authCAS=CAS";
        String body="";
        boolean state = true;
        while(state){
            body = hc.HTTPNormal(true, redirect, false, true, true, false);
            redirect = hc.getLocation();
            if(!redirect.equals("[]")){
                redirect = redirect.substring(redirect.indexOf("Location: ") + 10, redirect.indexOf("]"));
                System.out.println("\n\tRedirecting to "+redirect);
            }else{
                state = false;
            }
            
        }
        this.body = body;
    }
    
    public String getBody(){
        return this.body;
    }
}
