/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mdxnote;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.net.URLConnection;
import java.nio.file.Files;

/**
 *
 * @author nurma
 */
public class DownloadFile extends Thread{
    
    private String url, name;
    private final DownloadFileHandler dfh;
    private final int i;
    
    public DownloadFile(String url, String name, DownloadFileHandler dfh, int i){
        this.url = url;
        this.name = name;
        this.dfh = dfh;
        this.i = i;
    }
    
    @Override
    public void run(){
        System.out.println("Downloading file " + name + " @ " + url);
        
        
        String oldUrl = url;
        String contentType = "";
        ByteArrayOutputStream baos = null;
        boolean state = true;
        while(state){
            HTTPClient hc = new HTTPClient(false);
            hc.HTTPNormal(true, url, false, true, true, false);
            contentType = hc.getContentType();
            url = hc.getLocation();
            if(!url.isEmpty() && !url.equals("[]")){
                url = url.substring(url.indexOf("Location: ") + 10, url.indexOf("]"));
                oldUrl = url;
            }else{
                hc.HTTPNormal(true, oldUrl, true, true, true, true);
                contentType = hc.getContentType();
                System.out.println("\tResource located @ " + oldUrl + " and Content Type = " + contentType);
                baos = hc.getBaos(); 
                state = false;
            }
        }
        
         
        try {
            //writing file to local directory
            
            String contentTypeNew;
            if(!contentType.contains("text/html")){
                try{                
                    contentTypeNew = contentType.substring(contentType.indexOf(": ") + 2, contentType.indexOf(";"));
                }catch(StringIndexOutOfBoundsException e){
                    contentTypeNew = contentType.substring(contentType.indexOf(": ") + 2, contentType.indexOf("]"));
                }            
                String fileType = name.contains(".") ? "" : FileExtensionFromMIME.getExtension(contentTypeNew);

                System.out.println("Success downloading " + name + fileType + " @ " + oldUrl);
                File f = new File("data\\" + name + fileType);
                f.getParentFile().mkdirs();
                FileOutputStream fs =  new FileOutputStream(f, false);
                fs.write(baos.toByteArray());
                fs.close();
            }else{
                dfh.html(i);
            }
            
            
            dfh.success();
        } catch (IOException ex) {
            Logger.getLogger(DownloadFile.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
